
#define PWM_COUNT 3

static NRF_PWM_Type* pwms[PWM_COUNT] = {
  NRF_PWM0,
  NRF_PWM1,
  NRF_PWM2
};

static uint32_t pwmChannelPins[PWM_COUNT] = {
  0xFFFFFFFF,
  0xFFFFFFFF,
  0xFFFFFFFF
};
static uint16_t pwmChannelSequence[PWM_COUNT];

static int writeResolution = 8;

// #define PWM_PRESCALER_PRESCALER_DIV_1 (0UL) /*!< Divide by   1 (16MHz) */
// #define PWM_PRESCALER_PRESCALER_DIV_2 (1UL) /*!< Divide by   2 ( 8MHz) */
// #define PWM_PRESCALER_PRESCALER_DIV_4 (2UL) /*!< Divide by   4 ( 4MHz) */
// #define PWM_PRESCALER_PRESCALER_DIV_8 (3UL) /*!< Divide by   8 ( 2MHz) */
// #define PWM_PRESCALER_PRESCALER_DIV_16 (4UL) /*!< Divide by  16 ( 1MHz) */
// #define PWM_PRESCALER_PRESCALER_DIV_32 (5UL) /*!< Divide by  32 ( 500kHz) */
// #define PWM_PRESCALER_PRESCALER_DIV_64 (6UL) /*!< Divide by  64 ( 250kHz) */
// #define PWM_PRESCALER_PRESCALER_DIV_128 (7UL) /*!< Divide by 128 ( 125kHz) */

// Right now, PWM output only works on the pins with
// hardware support.  These are defined in the appropriate
// pins_*.c file.  For the rest of the pins, we default
// to digital output.
void myPWM( uint32_t ulPin, uint32_t ulValue, uint32_t ulCounter, uint8_t ulPrescaler)
{
  if (ulPin >= PINS_COUNT) {
    return;
  }

  if (ulPrescaler >= 8) {
    return;
  }


  ulPin = g_ADigitalPinMap[ulPin];

  for (int i = 0; i < PWM_COUNT; i++) {
    if (pwmChannelPins[i] == 0xFFFFFFFF || pwmChannelPins[i] == ulPin) {
      pwmChannelPins[i] = ulPin;
      pwmChannelSequence[i] = ulValue | bit(15);

      NRF_PWM_Type* pwm = pwms[i];

      pwm->PSEL.OUT[0] = ulPin;
      pwm->PSEL.OUT[1] = ulPin;
      pwm->PSEL.OUT[2] = ulPin;
      pwm->PSEL.OUT[3] = ulPin;
      pwm->ENABLE = (PWM_ENABLE_ENABLE_Enabled << PWM_ENABLE_ENABLE_Pos);
      pwm->PRESCALER = ulPrescaler;
      pwm->MODE = PWM_MODE_UPDOWN_Up;
      pwm->COUNTERTOP = ulCounter;
      pwm->LOOP = 0;
      pwm->DECODER = ((uint32_t)PWM_DECODER_LOAD_Common << PWM_DECODER_LOAD_Pos) | ((uint32_t)PWM_DECODER_MODE_RefreshCount << PWM_DECODER_MODE_Pos);
      pwm->SEQ[0].PTR = (uint32_t)&pwmChannelSequence[i];
      pwm->SEQ[0].CNT = 1;
      pwm->SEQ[0].REFRESH  = 1;
      pwm->SEQ[0].ENDDELAY = 0;
      pwm->TASKS_SEQSTART[0] = 0x1UL;

      break;
    }
  }
}

#define pwm_pin 8


void setup() {
  // put your setup code here, to run once:
  pinMode(pwm_pin, OUTPUT);
  myPWM(pwm_pin, 200, 500, 4);
}

void loop() {
  // put your main code here, to run repeatedly:

}
